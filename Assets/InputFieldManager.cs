﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

namespace SaveSystemExample
{
    public class InputFieldManager : MonoBehaviour
    {
        [SerializeField]
        private TMP_InputField inputField;

        private void Awake()
        {
            SaveSystem.InitialiseSaveSystem();
            SaveSystem.LoadData();
            inputField.text = SaveSystem.GetSaveReference().SavedText;
        }

        private void OnDisable()
        {
            SaveSystem.GetSaveReference().SavedText = inputField.text;
            SaveSystem.SaveData();
        }
    }
}