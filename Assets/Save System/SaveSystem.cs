﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

using SaveSystemExample.GameSaves;

namespace SaveSystemExample
{
    /// <summary>
    /// Additional binary serializers for some unity classes
    /// </summary>
    namespace Utils
    {
        public class Vector3SerializationSurrogate : ISerializationSurrogate
        {
            // Method called to serialize a Vector3 object
            public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
            {

                Vector3 v3 = (Vector3)obj;
                info.AddValue("x", v3.x);
                info.AddValue("y", v3.y);
                info.AddValue("z", v3.z);
            }

            // Method called to deserialize a Vector3 object
            public object SetObjectData(object obj, SerializationInfo info,
                                               StreamingContext context, ISurrogateSelector selector)
            {
                Vector3 v3 = (Vector3)obj;
                v3.x = (float)info.GetValue("x", typeof(float));
                v3.y = (float)info.GetValue("y", typeof(float));
                v3.z = (float)info.GetValue("z", typeof(float));
                obj = v3;
                return obj;
            }
        }
    }

    public static class SaveSystem
    {
        private static SaveData CurrentSaveState { get; set; }

        public static readonly string saveFileName = "save.data";
        public static string savePath;

        public static void InitialiseSaveSystem()
        {
#if UNITY_STANDALONE || UNITY_IOS || UNITY_ANDROID
            savePath = Application.persistentDataPath + "/" + saveFileName;
#elif UNITY_SWITCH
            // Censored due to licensing
#endif
        }

        /// <summary>
        /// A reference-based gettet for Save Data. 
        /// Should be used in pair with a value-based getter to avoid issues.
        /// This method exists for the ease of access to modify value-based properties or call functions.
        /// </summary>
        /// <returns>A reference to currently loaded save data</returns>
        public static SaveData GetSaveReference()
        {
            return CurrentSaveState;
        }

        /*
        public static SaveData GetSaveCopy()
        {
            return CurrentSaveState;
        }
        */

        public static bool SaveData()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            SurrogateSelector surrogateSelector = new SurrogateSelector();
            Utils.Vector3SerializationSurrogate vector3SS = new Utils.Vector3SerializationSurrogate();
            surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SS);
            formatter.SurrogateSelector = surrogateSelector;

#if UNITY_STANDALONE || UNITY_IOS || UNITY_ANDROID
            Debug.Log("Saving to: " + savePath);

            FileStream stream = new FileStream(savePath, FileMode.Create);

            formatter.Serialize(stream, CurrentSaveState);
            stream.Close();

            return true;
#elif UNITY_SWITCH
            // Censored due to licensing
#endif
        }

        public static void ResetSaveData()
        {
            // reset game data
            CurrentSaveState = new SaveData();

            // save the reset data
            SaveData();
        }

        public static SaveData LoadData()
        {
            //TODO: SettingsData sett = CurrentSaveState.Settings; // we don't save/load settings
            BinaryFormatter formatter = new BinaryFormatter();
            SurrogateSelector surrogateSelector = new SurrogateSelector();
            Utils.Vector3SerializationSurrogate vector3SS = new Utils.Vector3SerializationSurrogate();
            surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SS);
            formatter.SurrogateSelector = surrogateSelector;

#if UNITY_STANDALONE || UNITY_IOS || UNITY_ANDROID
            Debug.Log("Loading from: " + savePath);

            if (File.Exists(savePath))
            {

                FileStream stream = new FileStream(savePath, FileMode.Open);

                try
                {
                    CurrentSaveState = formatter.Deserialize(stream) as SaveData;
                    Debug.Log("Save data loaded successfully");
                }
                catch (Exception e)
                {
                    Debug.LogError("The following exception occurred while attempting to load the save file: " + e.Message + "\n Creating new clean save data.");
                    CurrentSaveState = new SaveData();
                }
                stream.Close();
            }
            else
            {
                Debug.LogWarning("No save data found under given path: " + savePath + "\n Creating new clean save data.");
                CurrentSaveState = new SaveData();
                SaveData();
            }
#elif UNITY_SWITCH
            // Censored due to licensing
#endif
            VerifySaveDataStructure();
            return CurrentSaveState;
        }

        private static void VerifySaveDataStructure()
        {
            // Create a reference save instance
            SaveData referenceSaveData = new SaveData();

            RepairSaveObjectStructure(CurrentSaveState, referenceSaveData);
        }

        /// <summary>
        /// Automatisation and abstraction of save verification process
        /// Restores nulls to default values if a save file from older version of developement gets loaded, to prevent errors
        /// </summary>
        private static void RepairSaveObjectStructure(object testedObject, object referenceObject)
        {
            foreach (FieldInfo fieldInfo in referenceObject.GetType().GetFields())
            {
                if (!fieldInfo.FieldType.IsValueType)
                {
                    try
                    {
                        if (fieldInfo.GetValue(referenceObject) != null)
                        {
                            if (fieldInfo.GetValue(testedObject) == null)
                            {
                                Debug.LogWarning("Loaded savefile was form a previous version of the game and missed " + fieldInfo.Name + " field.\n Restoring initial field value.");
                                fieldInfo.SetValue(testedObject, fieldInfo.GetValue(referenceObject));
                                SaveData();
                            }
                            else
                            {
                                // Recursively checking for existence of leftover null fields
                                // Needed for custom types only
                                if (fieldInfo.FieldType.Namespace.StartsWith(nameof(SaveSystemExample)))
                                {
                                    RepairSaveObjectStructure(fieldInfo.GetValue(testedObject), fieldInfo.GetValue(referenceObject));
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Encountered an exception " + e.Message + " while processing field \"" + fieldInfo.Name + "\" of type "+ referenceObject.GetType().Name + " in the old save file.\n Restoring initial field value.");
                        fieldInfo.SetValue(testedObject, fieldInfo.GetValue(referenceObject));
                        SaveData();
                    }
                }
            }

            foreach (PropertyInfo propertyInfo in referenceObject.GetType().GetProperties())
            {
                if (!propertyInfo.PropertyType.IsValueType)
                {
                    try
                    {
                        if (propertyInfo.GetValue(referenceObject, null) != null)
                        {
                            if (propertyInfo.GetValue(testedObject) == null)
                            {
                                Debug.LogWarning("Loaded savefile was form a previous version of the game and missed " + propertyInfo.Name + " property.\n Restoring initial property value.");
                                propertyInfo.SetValue(testedObject, propertyInfo.GetValue(referenceObject));
                                SaveData();
                            }
                            else
                            {
                                // Recursively checking for existence of leftover null fields
                                // Needed for custom types only
                                if (propertyInfo.PropertyType.Namespace.StartsWith(nameof(SaveSystemExample)))
                                {
                                    RepairSaveObjectStructure(propertyInfo.GetValue(testedObject), propertyInfo.GetValue(referenceObject));
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Encountered an exception " + e.Message + " while processing property \"" + propertyInfo.Name +"\" of type "+ referenceObject.GetType().Name + " in the old save file.\n Restoring initial property value.");
                        propertyInfo.SetValue(testedObject, propertyInfo.GetValue(referenceObject));
                        SaveData();
                    }
                }
            }
        }
    }
}