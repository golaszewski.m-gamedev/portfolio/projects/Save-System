﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SaveSystemExample.GameSaves
{
    [System.Serializable]
    public class SaveData
    {
        // All saved data goes here
        public string SavedText { get; set; }

        public SaveData()
        {
            // Default save data values
            SavedText = "This is the default text value!\nGo on, change it to your liking, and exit play mode! :)";
        }
    }
}