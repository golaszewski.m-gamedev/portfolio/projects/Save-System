# Save System
A Game Save system utilising binary serialization to prevent plaintext manipulation and provide cross-platform compatibility.

As C# documentation metions the end of support for binary serialization in the future, the system might get rewritten to a scrambled/encrypted `.json` serializer instead.
